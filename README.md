Requerimientos mínimos:
1.- Clonar el proyecto
2.- Crear una entidad de persona(Nombre y teléfono)
3.- Crear función con un listado de personas
4.- Crear función para crear personas
5.- Crear función para eliminar personas
6.- Crear función con el detalle de la persona

- Se valorará el modelo de programación
- Se valorará el uso de Form de symfony
- Se valorará el uso de librerías como bootstrap, etc

Extra:
- Crear entidad de usuario (Nombre, usuario y contraseña)
- Crear login de acceso al contenido
- Configurar roles
